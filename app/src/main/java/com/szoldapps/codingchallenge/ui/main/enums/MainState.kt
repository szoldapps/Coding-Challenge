package com.szoldapps.codingchallenge.ui.main.enums

enum class MainState {
    INITIAL, LOADING, DONE, ERROR, DELETE_SUCCESS, DELETE_ERROR
}
