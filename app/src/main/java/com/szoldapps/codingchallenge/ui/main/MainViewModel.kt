package com.szoldapps.codingchallenge.ui.main

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.szoldapps.codingchallenge.service.model.User
import com.szoldapps.codingchallenge.ui.main.enums.MainState
import kotlinx.coroutines.experimental.launch


class MainViewModel(
    private val mainRepository: MainRepository
) : ViewModel() {

    val state = MutableLiveData<MainState>()
    val user = MutableLiveData<User>()

    init {
        state.value = MainState.INITIAL
    }

    fun loadUser() {
        launch {
            state.postValue(MainState.LOADING)
            mainRepository.getUser()?.let {
                user.postValue(it)
                state.postValue(MainState.DONE)
                return@launch
            }
            // something went wrong
            state.postValue(MainState.ERROR)
        }
    }

    fun deleteUser() {
        launch {
            state.postValue(MainState.LOADING)
            val wasUserSuccessfullyDeleted = user.value?.let {
                mainRepository.deleteUser(it.id)
            } ?: false
            if (wasUserSuccessfullyDeleted) {
                state.postValue(MainState.DELETE_SUCCESS)
            } else {
                state.postValue(MainState.DELETE_ERROR)
            }
        }
    }

}
