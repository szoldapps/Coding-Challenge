package com.szoldapps.codingchallenge.ui.main

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.squareup.picasso.Picasso
import com.szoldapps.codingchallenge.R
import com.szoldapps.codingchallenge.databinding.ActivityMainBinding
import com.szoldapps.codingchallenge.ui.main.enums.MainState
import kotlinx.android.synthetic.main.activity_main.btDelete
import kotlinx.android.synthetic.main.activity_main.btReload
import kotlinx.android.synthetic.main.activity_main.civUser
import kotlinx.android.synthetic.main.activity_main.tvLoading
import org.jetbrains.anko.design.indefiniteSnackbar
import org.jetbrains.anko.sdk25.listeners.onClick
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val viewModel by inject<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val databinding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        databinding.viewModel = viewModel
        // connect LiveData objects with databinding
        databinding.setLifecycleOwner(this)

        viewModel.state.observe(this, Observer {
            when (it) {
                MainState.DONE -> {
                    // load image
                    viewModel.user.value?.profilePicture?.let { profilePictureUrl ->
                        Picasso.get()
                            .load(profilePictureUrl)
                            .noFade()
                            .placeholder(R.drawable.ic_launcher_background)
                            .error(R.drawable.ic_launcher_background)
                            .into(civUser)
                    }
                }
                MainState.ERROR -> {
                    indefiniteSnackbar(
                        view = tvLoading,
                        message = getString(R.string.error_snackbar_message),
                        actionText = getString(R.string.error_snackbar_retry)
                    ) {
                        viewModel.loadUser()
                    }
                }
                MainState.DELETE_SUCCESS -> {
                    toast("User successfully deleted!")
                }
                MainState.DELETE_ERROR -> {
                    toast("User could not be deleted!")
                }
                else -> {
                    // do nothing
                }
            }
        })

        // delete button handling
        btDelete.onClick {
            viewModel.deleteUser()
        }

        // reload button handling
        btReload.onClick {
            viewModel.loadUser()
        }

        // load user
        viewModel.loadUser()

    }
}
