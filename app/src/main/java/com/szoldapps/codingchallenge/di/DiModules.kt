package com.szoldapps.codingchallenge.di

import com.szoldapps.codingchallenge.service.api.UserService
import com.szoldapps.codingchallenge.ui.main.MainRepository
import com.szoldapps.codingchallenge.ui.main.MainViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory


/**
 * Koin module
 */
val lidlAppModule: Module =
    applicationContext {
        bean { MainRepository(get<UserService>()) }
        bean {
            Retrofit.Builder()
                .baseUrl("https://private-anon-4e0fe93fb3-test16231.apiary-mock.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(UserService::class.java)
        }
        viewModel { MainViewModel(get<MainRepository>()) }
    }