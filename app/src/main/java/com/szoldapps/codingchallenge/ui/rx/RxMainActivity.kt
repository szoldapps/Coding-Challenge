package com.szoldapps.codingchallenge.ui.rx

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.szoldapps.codingchallenge.R
import com.szoldapps.codingchallenge.service.api.UserService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_rx_main.textView
import org.koin.android.ext.android.inject

class RxMainActivity : AppCompatActivity() {

    private var disposable: Disposable? = null
    private val userService by inject<UserService>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rx_main)

        disposable = userService.getUserObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { user -> textView.text = user.firstName + " " + user.lastName },
                { error -> Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show() }
            )

    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}
