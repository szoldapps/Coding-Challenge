The goal is to create a simple app that fetches User information from a mock API and display it on the screen.

Resources:

https://test16231.docs.apiary.io/#reference  (please switch to Mockserver to get the correct request URLs, from "Switch to exmaple" button on the top right)

The mocked API is based around User entities and currently supports only two methods:
- One for retrieving the information
- One for deleting the user profile (please note that since this is a mocked API the entities will not really be affected)

All the information about the User entity can be found in the documentation provided above.

The app will have the following features:

- When opened, the User information is displayed on the screen. The user’s avatar should be displayed using a round image view.
- There will be a “delete” button on the bottom of the screen that when pressed a delete request is made to the mock api to delete the user ’s phone number. A toast will be shown afterwards to display a success/fail message.
- The design of the application is left to the candidate to decide.
- Both native components as well as custom ones can be used
- Unit tests if possible.

Please create a git repository and send a link for your solution, if you have any questions don’t hesitate to ask.

This challenge is to give us a better idea of your coding style, please feel free to use any libraries you like.