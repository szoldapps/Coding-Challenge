package com.szoldapps.codingchallenge.ui.main

import com.szoldapps.codingchallenge.service.api.UserService
import com.szoldapps.codingchallenge.service.model.User
import java.io.IOException


class MainRepository(private val userService: UserService) {

    fun getUser(): User? {
        return try {
            userService.getUser()?.execute()?.body()
        } catch (exception: IOException) {
            return null
        }
    }

    fun deleteUser(id: String) =
        try {
            userService.deleteUser(id).execute().isSuccessful
        } catch (exception: IOException) {
            false
        }

}
