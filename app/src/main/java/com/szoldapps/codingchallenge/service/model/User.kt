package com.szoldapps.codingchallenge.service.model

data class User(
    var id: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var phoneNumber: String = "",
    var email: String = "",
    var profilePicture: String = ""
)
