package com.szoldapps.codingchallenge.service.api

import com.szoldapps.codingchallenge.service.model.User
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Path


interface UserService {

    @GET("user/all")
    fun getUser(): Call<User>?

    @GET("user/all")
    fun getUserObservable(): Observable<User>

    @DELETE("user/{id}")
    fun deleteUser(@Path("id") id: String): Call<ResponseBody>

}
