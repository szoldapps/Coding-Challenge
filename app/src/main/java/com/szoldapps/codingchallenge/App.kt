package com.szoldapps.codingchallenge

import android.app.Application
import com.szoldapps.codingchallenge.di.lidlAppModule
import org.koin.android.ext.android.startKoin

class App() : Application() {

    override fun onCreate() {
        super.onCreate()

        // initialize Koin
        startKoin(this, listOf(lidlAppModule))
    }
}
